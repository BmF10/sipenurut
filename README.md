# sipenurut

Si Penurut (Sistem Informasi Pencari Guru Private )

![Repo_List](assets/users/images/home/SiPenurut.png)

## Installation

Download dan ekstrak ke htdocs,

Buat database dengan nama "sipenurut" dan import file database.sql

```bash
sipenurut
```

## Usage

Jalankan dengan [http://localhost/sipenurut](http://localhost/sipenurut)

Login Admin
```bash

Superadmin
Username : Superadmin
Password : 123456

Admin
Username : Admin
Password : 123456
```

