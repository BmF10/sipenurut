/*
SQLyog Enterprise v12.09 (64 bit)
MySQL - 10.4.8-MariaDB : Database - sipenurut
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sipenurut` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `sipenurut`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_admin` varchar(100) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `superadmin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `ajar` */

DROP TABLE IF EXISTS `ajar`;

CREATE TABLE `ajar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_permintaan` int(11) DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `status` enum('Aktif','Selesai') DEFAULT NULL,
  `review` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `guru` */

DROP TABLE IF EXISTS `guru`;

CREATE TABLE `guru` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_guru` varchar(100) DEFAULT NULL,
  `tmpt_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `agama` enum('Islam','Kristen','Katolik','Buddha','Hindu','Konghucu') DEFAULT NULL,
  `jenjang` enum('SD','SMP','SMA') DEFAULT NULL,
  `jurusan` enum('IPA','IPS') DEFAULT NULL,
  `pddterakhir` enum('Sedang Kuliah','D3','S1/D4','S2','S3') DEFAULT NULL,
  `jurusanpdd` varchar(100) DEFAULT NULL,
  `ijazah` text DEFAULT NULL,
  `nilai` text DEFAULT NULL,
  `foto` text DEFAULT NULL,
  `verifikasi` enum('Belum Terverifikasi','Terverifikasi','Permintaan Ditolak') DEFAULT NULL,
  `id_wilayah` int(11) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `jns_kel` enum('Laki-laki','Perempuan') DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_wilayah` (`id_wilayah`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `murid` */

DROP TABLE IF EXISTS `murid`;

CREATE TABLE `murid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_murid` varchar(100) DEFAULT NULL,
  `jns_kel` enum('Laki-laki','Perempuan') DEFAULT NULL,
  `tmpt_lahir` varbinary(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `agama` enum('Islam','Kristen','Katolik','Buddha','Hindu','Konghucu') DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `permintaan` */

DROP TABLE IF EXISTS `permintaan`;

CREATE TABLE `permintaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_murid` int(11) DEFAULT NULL,
  `id_guru` int(11) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `jumlah` int(3) DEFAULT NULL,
  `hari` varchar(100) DEFAULT NULL,
  `status` enum('Terima','Tolak','Proses') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `review` */

DROP TABLE IF EXISTS `review`;

CREATE TABLE `review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ajar` int(11) DEFAULT NULL,
  `id_guru` int(11) DEFAULT NULL,
  `id_murid` int(11) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `ulasan` text DEFAULT NULL,
  `rate` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `verifikasi` */

DROP TABLE IF EXISTS `verifikasi`;

CREATE TABLE `verifikasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_guru` int(11) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  `status` enum('Proses','Terima','Tolak') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `wilayah` */

DROP TABLE IF EXISTS `wilayah`;

CREATE TABLE `wilayah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kota` varchar(50) DEFAULT NULL,
  `provinsi` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

INSERT INTO admin(nm_admin,username,PASSWORD,superadmin) VALUES('Admin','Admin',MD5(123456),0);
INSERT INTO admin(nm_admin,username,PASSWORD,superadmin) VALUES('Super Admin','Superadmin',MD5(123456),1);


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
